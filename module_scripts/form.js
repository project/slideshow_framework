
var slideshowFrameworkForm = {};
slideshowFrameworkForm.interval = false;

function slideshowFrameworkEnableUpdateButtons(button)
{
	if(!button.is(".progress-disabled"))
	{
		clearInterval(slideshowFrameworkForm.interval);
		$("#slideshow_images_wrapper .form-submit").attr("disabled", "");
	}
};

function slideshowFrameworkEnableAddButton(button)
{
	if(!button.is(".progress-disabled"))
	{
		clearInterval(slideshowFrameworkForm.interval);
		$("#edit-add-slideshow-image").attr("disabled", "");
	}
};

Drupal.behaviors.slideshowFrameworkForm = function()
{
	$("#slideshow_images_wrapper .form-submit").each(function()
	{
		$(this).ajaxComplete(function()
		{
			$("#edit-add-slideshow-image").removeAttr("disabled");
		});
		$(this).mousedown(function()
		{
			$("#edit-add-slideshow-image").attr("disabled", "disabled");
		});
	});
	$("#edit-add-slideshow-image").mousedown(function()
	{
		$("#slideshow_images_wrapper .form-submit").attr("disabled", "disabled");
		$(this).ajaxComplete(function()
		{
			$("#slideshow_images_wrapper .form-submit").removeAttr("disabled");
		});
	});
};