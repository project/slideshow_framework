<?php
/**
 * @file node-slideshow-image-titles.tpl.php
 *
 * This template handles the layout of the the slideshow image titles
 *
 * Variables available:
 * - $image_titles: An array of all the image titles in the same order as the images.
 */
?>
<div id="slideshow_framework_image_titles">
	<?php foreach ($image_titles as $title): ?>
		<?php print $title; ?>
	<?php endforeach; ?>
</div>