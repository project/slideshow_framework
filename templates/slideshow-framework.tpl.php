<?php
/**
 * @file node-slideshow.tpl.php
 *
 * This template handles the layout of the slideshow.
 *
 * Variables available:
 * - $display_slideshow: This value is TRUE if slideshow data exists, and FALSE if it doesn't.
 * - $teaser: This value is TRUE if the slideshow is being displayed in a teaser, FALSE if it's being displayed on the node display page.
 * - $selected_image: This will be the currently selected image. If no image has been selected, it will be the first image in the slideshow.
 * - $thumbs: The thumbnails for the images in the slideshow. Thumbnails have already been rendered in node-slideshow-thumb.tpl.php and node-slideshow-thumbnails.tpl.php
 * - $preload_images: This will be all of the full sized images in the slideshow, allowing for preloading of the images before they are to be displayed. Setting the CSS of the containing div to display:none will hide the images, but they will still be loaded.
 */
?>
<?php if($display_slideshow && !$teaser): ?>
	<div id="slideshow_framework">
		<?php if($titles_exist): ?>
			<?php print $image_titles; ?>
		<?php endif; ?>
		<div id="slideshow_framework_images">
			<?php print $thumbs; ?>
			<div id="selected_image"><?php print $selected_image; ?></div>
			<?php if($preload_images): ?>
				<div id="preloaded_images"><?php print $preload_images; ?></div>
			<?php endif; ?>
		</div>
		<?php if($descriptions_exist): ?>
			<?php print $descriptions; ?>
		<?php endif; ?>
		<?php if($allow_downloads): ?>
			<p id="download_explanation"><?php print t('To download a full sized image, hover over the thumbnail for the image and click the download link that pops up.'); ?></p>
		<?php endif; ?>
	</div>
<?php elseif($teaser): ?>
	<?php print $selected_image; ?>
<?php endif; ?>