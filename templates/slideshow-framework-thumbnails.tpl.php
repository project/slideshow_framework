<?php
/**
 * @file node-slideshow-thumbnails.tpl.php
 *
 * This template handles the layout of the the slideshow thumbnails
 *
 * Variables available:
 * - $thumbnails: This value is TRUE if slideshow data exists, and FALSE if it doesn't.
 * - $teaser: This value is TRUE if the slideshow is being displayed in a teaser, FALSE if it's being displayed on the node display page.
 */
?>
<div id="thumbs">
	<div id="slider">
		<?php foreach($thumbnails as $thumb): ?>
			<?php print $thumb; ?>
		<?php endforeach; ?>
	</div>
</div>