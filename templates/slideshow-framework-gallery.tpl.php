<?php
/**
 * @file node-slideshow-gallery.tpl.php
 *
 * This template handles the display of the gallery page.
 *
 * Variables available:
 * - $slideshows: An array of slideshow data. Each item of the array will contain the following:
 *    - link: A link to the node that contains the slideshow
 *    - title: The title of the slideshow
 *    - creation_date: The date on which the slideshow was created
 *    - username: The username of the user who created the node the slideshow is attached to
 *    - uid: The UID of the user who created the node the slideshow is attached to
 *    - thumb: A thumbnail of the first image in the slideshow
 * - $pager: A pager for when the number of slideshows exceeds the number of slideshows per page
 */
?>
<?php if($slideshows): ?>
	<ul class="slideshow_listings">
		<?php foreach($slideshows as $slideshow): ?>
			<li>
				<a href="<?php print $slideshow['link']; ?>"><?php print $slideshow['thumb']; ?></a>
				<p><a href="<?php print $slideshow['link']; ?>"><?php print $slideshow['title']; ?></a></p>
				<p class="slideshow_date"><?php print $slideshow['creation_date']; ?></p>
				<p class="slideshow_creator"><a href="/<?php print 'user/' . $slideshow['uid']; ?>"><?php print $slideshow['username']; ?></a></p>
			</li>
		<?php endforeach; ?>
	</ul>
	<?php print $pager; ?>
<?php else: ?>
	<p><?php print t('No slideshows have been created'); ?></p>
<?php endif; ?>