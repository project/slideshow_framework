<?php
/**
 * @file node-slideshow-image-descriptions.tpl.php
 *
 * This template handles the layout of the the slideshow image descriptions
 *
 * Variables available:
 * - $descriptions: An array of all the image descriptions in the same order as the images.
 */
?>
<div id="slideshow_framework_image_descriptions">
	<?php foreach ($descriptions as $description): ?>
		<?php print $description; ?>
	<?php endforeach; ?>
</div>