<?php
/**
 * @file node-slideshow-thumb.tpl.php
 *
 * This template handles the layout of individual thumbnails.
 *
 * Variables available:
 * - $image: The thumbnail image
 * - $selected: Indicates whether the image is the selected image or not. This is used for a non-javascript fallback. The value will be true or false.
 * - $link: A link to the slideshow, making the clicked thumbnail the selected image when the page re-loads. This is used for a non-javascript fallback.
 * - $download link: A direct link to download the image. If downloads are turned off, this will be false, otherwise it will be the link to the image.
 */
?>
<div class="thumb<?php if($selected){ print ' selected';} ?>">
	<a href="<?php print $link; ?>"><?php print $image; ?></a>
	<?php if($download_link): ?>
		<a href="<?php print $download_link; ?>" class="image_download_link"><?php print t('Download'); ?></a>
	<?php endif; ?>
</div>