<?php

function slideshow_framework_install()
{
	$path = variable_get('file_directory_path', 'sites/default/files') . '/slideshow_framework_import';
	file_check_directory($path, TRUE);
	$imagecachepreset = imagecache_preset_save(array('presetname' => 'slideshow_framework_image'));
	// Action
	$imagecacheaction = new stdClass ();
	$imagecacheaction->presetid = $imagecachepreset['presetid'];
	$imagecacheaction->module = 'imagecache';
	$imagecacheaction->action = 'imagecache_scale';
	$imagecacheaction->data = array('width' => '800', 'height' => '600' );
	drupal_write_record('imagecache_action', $imagecacheaction);
	
	$imagecachepreset = imagecache_preset_save(array('presetname' => 'slideshow_framework_thumb'));
	// Action
	$imagecacheaction = new stdClass ();
	$imagecacheaction->presetid = $imagecachepreset['presetid'];
	$imagecacheaction->module = 'imagecache';
	$imagecacheaction->action = 'imagecache_scale_and_crop';
	$imagecacheaction->data = array('width' => '110', 'height' => '77' );
	drupal_write_record('imagecache_action', $imagecacheaction);
	drupal_install_schema('slideshow_framework');
}

function slideshow_framework_uninstall()
{
	imagecache_preset_delete(imagecache_preset_by_name('slideshow_framework_image'));
	imagecache_preset_delete(imagecache_preset_by_name('slideshow_framework_thumb'));
	drupal_uninstall_schema('slideshow_framework');
}

function slideshow_framework_schema()
{
	$schema['slideshow_framework_slideshows'] = array
	(
		'description' => t('Holds slideshow data'),
		'fields' => array
		(
			'nid' => array
			(
				'description' => t('The NID from the {node} table'),
				'type' => 'int',
				'unsigned' => TRUE,
				'not null' => TRUE,
			),
			'allow_downloads' => array
			(
				'description' => t('A boolean indicating whether images in the slideshow should be downloadable'),
				'type' => 'int',
				'not null' => TRUE,
				'length' => 1,
			),
			'private_downloads' => array
			(
				'description' => t('A boolean indicating whether image downloads should only be available to users with permission to view the node in question'),
				'type' => 'int',
				'not null' => TRUE,
				'length' => 1,
			),
		),
		'primary key' => array('nid'),
	);
	$schema['slideshow_framework_images'] = array
	(
		'description' => t('Holds data on slideshow images'),
		'fields' => array
		(
			'nid' => array
			(
				'description' => t('The NID from the {node} table'),
				'type' => 'int',
				'unsigned' => TRUE,
				'not null' => TRUE,
			),
			'fid' => array
			(
				'description' => t('The FID from the {files} table'),
				'type' => 'int',
				'unsigned' => TRUE,
				'not null' => TRUE,
			),
			'alt_text' => array
			(
				'description' => t('Alt text for slideshow images'),
				'type' => 'varchar',
				'length' => 128,
				'default' => NULL,
			),
			'title_text' => array
			(
				'description' => t('Title text for slideshow images'),
				'type' => 'varchar',
				'length' => 128,
				'default' => NULL,
			),
			'image_title' => array
			(
				'description' => t('The title of the image'),
				'type' => 'varchar',
				'length' => 128,
				'default' => NULL,
			),
			'description' => array
			(
				'description' => t('Image description text'),
				'type' => 'text',
				'default' => NULL,
			),
			'weight' => array
			(
				'description' => t('Sets the position of each image in a list'),
				'type' => 'int',
				'not null' => TRUE,
				'default' => 0,
			),
			'hash' => array
			(
				'description' => t('A hashed value, allowing for secure downloads of images'),
				'type' => 'varchar',
				'not null' => TRUE,
				'length' => 40,
			),
		),
		'primary key' => array('nid', 'fid'),
		'indexes' => array
		(
			'hash' => array('hash'),
			'weight' => array('fid', 'weight'),
		),
	);
	return $schema;
}

function slideshow_framework_update_6000()
{
	$ret = array();
	db_create_table
	(
		$ret,
		'slideshow_framework_images',
		array
		(
			'description' => t('Joining table between {files} and {node} for nodes that have slideshow images'),
			'fields' => array
			(
				'nid' => array
				(
					'description' => t('The NID from the {node} table'),
					'type' => 'int',
					'unsigned' => TRUE,
					'not null' => TRUE,
				),
				'fid' => array
				(
					'description' => t('The FID from the {files} table'),
					'type' => 'int',
					'unsigned' => TRUE,
					'not null' => TRUE,
				),
			),
			'primary key' => array('nid', 'fid'),
		)
	);
	return $ret;
}

function slideshow_framework_update_6001()
{
	$ret = array();
	db_add_field
	(
		$ret,
		'slideshow_framework_images',
		'hash',
		array
		(
			'description' => t('A hashed value, allowing for secure downloads of images'),
			'type' => 'varchar',
			'not null' => TRUE,
			'length' => 40,
			'default' => '',
		)
	);
	return $ret;
}

function slideshow_framework_update_6002()
{
	$ret = array();
	$db_items = db_query('SELECT nid, field_slideshow_framework_image_fid AS fid FROM {content_field_slideshow_framework_image}');
	$query1 = 'INSERT INTO {slideshow_framework_images} (nid, fid, hash) VALUES ';
	$query2 = '';
	while($item = db_fetch_array($db_items))
	{
		if(strlen($query2))
		{
			$query2 .= ',';
		}
		$query2 .= '(%d, %d, "%s")';
		$arguments[] = $item['nid'];
		$arguments[] = $item['fid'];
		$hash = sha1(_slideshow_framework_str_rand());
		$hash_exists = db_result(db_query('SELECT 1 FROM {slideshow_framework_images} WHERE hash = "%s" LIMIT 1', $hash));
		while($hash_exists)
		{
			$hash = sha1(_slideshow_framework_str_rand());
			$hash_exists = db_result(db_query('SELECT 1 FROM {slideshow_framework_images} WHERE hash = "%s" LIMIT 1', $hash));
		}
		$arguments[] = $hash;
	}
	if(strlen($query2))
	{
		db_query($query1 . $query2, $arguments);
	}
	return $ret;
}

function slideshow_framework_update_6003()
{
	$ret = array();
	db_add_index
	(
		$ret,
		'slideshow_framework_images',
		'hash',
		array('hash')
	);
	return $ret;
}

function slideshow_framework_update_6004()
{
	$ret = array();
	db_add_field
	(
		$ret,
		'slideshow_framework_images',
		'weight',
		array
		(
			'description' => t('Sets the position of each image in a list'),
			'type' => 'int',
			'not null' => TRUE,
			'default' => 0,
		),
		array
		(
			'indexes' => array
			(
				'weight' => array('fid', 'weight'),
			),
		)
	);
	return $ret;
}


