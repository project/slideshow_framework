<?php

/** 
 * Implementation of hook_perm()
 */
function slideshow_framework_perm()
{
	return array
	(
		'Administer slideshow framework',
		'Access slideshow gallery',
		'Import slideshow images',
	);
}

/**
 * Implementation of hook_menu()
 */
function slideshow_framework_menu()
{
	$menu['admin/settings/slideshow_framework'] = array
	(
		'title' => 'Slideshow Framework',
		'page callback' => 'drupal_get_form',
		'page arguments' => array('slideshow_framework_settings'),
		'access arguments' => array('Administer slideshow framework'),
		'file' => 'slideshow_framework.pages.inc',
	);
	$menu['admin/settings/slideshow_framework/slideshow_framework'] = array
	(
		'title' => 'Settings',
		'access arguments' => array('Administer slideshow framework'),
		'type' => MENU_DEFAULT_LOCAL_TASK,
	);
	$menu['admin/content/slideshow_framework_image_import'] = array
	(
		'title' => 'Slideshow Framework Image Import',
		'page callback' => 'drupal_get_form',
		'page arguments' => array('slideshow_framework_image_import'),
		'access arguments' => array('Import slideshow images'),
		'file' => 'slideshow_framework.pages.inc',
	);
	$menu['gallery'] = array
	(
		'title' => 'Gallery',
		'access arguments' => array('Access slideshow gallery'),
		'page callback' => 'slideshow_framework_gallery',
		'file' => 'slideshow_framework.pages.inc',
	);
	$menu['slideshow_framework/ahah_callback'] = array
	(
		'title' => 'Node Slideshow AHAH Callback',
		'page callback' => 'slideshow_framework_add_image_element',
		'page arguments' => array(2),
		'access callback' => TRUE,
		'type' => MENU_CALLBACK,
	);
	$menu['slideshow_framework/download/%node/%'] = array
	(
		'title' => 'Node Slideshow Download',
		'page callback' => 'slideshow_framework_download',
		'page arguments' => array(3),
		'access callback' => 'slideshow_framework_download_access_callback',
		'access arguments' => array(2),
		'type' => MENU_CALLBACK,
		'file' => 'slideshow_framework.pages.inc',
	);
	return $menu;
}

/**
 * Access callback function for the path 'slideshow_framework/download/%node/%'
 * This function checks to see if images are set up on private download or not.
 * If image downloads are set to private download, only users who have
 * access grants to view the node in question will be allowed to download the image.
 * If image downloads are not set to private, all users will be given permission to 
 * download the image
 */
function slideshow_framework_download_access_callback($node)
{
	if($node->slideshow_allow_downloads && $node->slideshow_private_downloads)
	{
		global $user;
		return node_access('view', $node, $user);
	}
	return TRUE;
}

/**
 * Implementation of hook_nodeapi
 */
function slideshow_framework_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL)
{
	$types = variable_get('slideshow_framework_content_types', array());
	if(in_array($node->type, $types, TRUE))
	{
		if($op == 'load')
		{
			$fids = db_query('SELECT fid FROM {slideshow_framework_images} WHERE nid = %d ORDER BY weight', $node->nid);
			while($fid = db_fetch_array($fids))
			{
				$node->slideshow_framework_fids[] = $fid['fid'];
			}
			$download_data = db_fetch_array(db_query('SELECT allow_downloads, private_downloads FROM {slideshow_framework_slideshows} WHERE nid = %d', $node->nid));
			$node->slideshow_allow_downloads = $download_data['allow_downloads'];
			$node->slideshow_private_downloads = $download_data['private_downloads'];
		}
		elseif($op == 'view')
		{
			$image_data = array();
			if(count($node->slideshow_framework_fids))
			{
				$db_image_data = db_query
				(
					'SELECT f.fid, f.filepath, sfi.alt_text, sfi.title_text, sfi.image_title, sfi.description, sfi.hash ' .
					'FROM {files} AS f ' .
					'JOIN {slideshow_framework_images} AS sfi ' .
					'ON sfi.fid = f.fid ' .
					'WHERE f.fid IN (%s) ' .
					'ORDER BY sfi.weight',
					implode(',', $node->slideshow_framework_fids)
				);
				while($image = db_fetch_array($db_image_data))
				{
					$data = unserialize($image['field_product_image_data']);
					$image_data[$image['fid']] = array
					(
						'filepath' => $image['filepath'],
						'hash' => $image['hash'],
						'alt_text' => $image['alt_text'],
						'title_text' => $image['title_text'],
						'image_title' => $image['image_title'],
						'description' => $image['description'],
					);
				}
				$selected_image_id = ($_GET['image']) ? $_GET['image'] : FALSE;
				
				$node->content['slideshow'] = array
				(
					'#weight' => -2,
					'#value' => theme('slideshow_framework', $image_data, $node->slideshow_allow_downloads, $selected_image_id, $node->nid, $a3),
				);
				
			}
		}
		elseif($op == 'insert')
		{
			$query1 = 'INSERT INTO {slideshow_framework_images} (nid, fid, alt_text, title_text, image_title, description, weight, hash) VALUES ';
			$query2 = '';
			$arguments = array();
			foreach(element_children($node) as $index)
			{
				if(preg_match('/^slideshow_framework_image_([0-9]+)$/', $index, $matches))
				{
					if($node->$matches[0]->submit_action)
					{
						$image_id = 0;
						$image_id = upload_element_save($node->$matches[0], 'slideshow_framework_images', FILE_EXISTS_RENAME);
						if($image_id)
						{
							if(strlen($query2))
							{
								$query2 .= ',';
							}
							$query2 .= '(%d, %d, "%s", "%s", "%s", "%s", %d, "%s")';
							$arguments[] = $node->nid;
							$arguments[] = $image_id;
							$alt_index = 'slideshow_framework_alt_text_' . $matches[1];
							$arguments[] = $node->$alt_index;
							$title_index = 'slideshow_framework_title_text_' . $matches[1];
							$arguments[] = $node->$title_index;
							$image_title_index = 'slideshow_framework_image_title_' . $matches[1];
							$arguments[] = $node->$image_title_text;
							$description_index = 'slideshow_framework_description_' . $matches[1];
							$arguments[] = $node->$description_index;
							$weight_index = 'weight_' . $matches[1];
							$arguments[] = $node->$weight_index;
							$hash = sha1(_slideshow_framework_str_rand());
							$hash_exists = db_result(db_query('SELECT 1 FROM {slideshow_framework_images} WHERE hash = "%s" LIMIT 1', $hash));
							while($hash_exists)
							{
								$hash = sha1(_slideshow_framework_str_rand());
								$hash_exists = db_result(db_query('SELECT 1 FROM {slideshow_framework_images} WHERE hash = "%s" LIMIT 1', $hash));
							}
							$arguments[] = $hash;
						}
					}
				}
				// This next section is executed when using the import function
				elseif($index == 'slideshow_framework_fids' && is_array($node->slideshow_framework_fids))
				{
					$count = count($node->slideshow_framework_fids);
					for($i = 0; $i < $count; $i++)
					{
						if(strlen($query2))
						{
							$query2 .= ',';
						}
						$query2 .= '(%d, %d, "%s", "%s", "%s", "%s", %d, "%s")';
						$arguments[] = $node->nid;
						$arguments[] = $node->slideshow_framework_fids[$i];
						$arguments[] = $node->slideshow_framework_alt_texts[$i];
						$arguments[] = $node->slideshow_framework_title_texts[$i];
						$arguments[] = $node->slideshow_framework_image_titles[$i];
						$arguments[] = $node->slideshow_framework_descriptions[$i];
						$arguments[] = 0;
						$hash = sha1(_slideshow_framework_str_rand());
						$hash_exists = db_result(db_query('SELECT 1 FROM {slideshow_framework_images} WHERE hash = "%s" LIMIT 1', $hash));
						while($hash_exists)
						{
							$hash = sha1(_slideshow_framework_str_rand());
							$hash_exists = db_result(db_query('SELECT 1 FROM {slideshow_framework_images} WHERE hash = "%s" LIMIT 1', $hash));
						}
						$arguments[] = $hash;
					}
				}
			}
			if(strlen($query2))
			{
				db_query($query1 . $query2, $arguments);
			}
			db_query('INSERT INTO {slideshow_framework_slideshows} (nid, allow_downloads, private_downloads) VALUES (%d, %d, %d)', $node->nid, $node->slideshow_framework_download_link, $node->slideshow_framework_secure_downloads);
		}
		elseif($op == 'update')
		{
			$current_db_data = db_query('SELECT fid, alt_text, title_text, image_title, description, weight FROM {slideshow_framework_images} WHERE nid = %d', $node->nid);
			$weight_data = array();
			while($data = db_fetch_array($current_db_data))
			{
				$current_image_data[$data['fid']] = array
				(
					'alt_text' => $data['alt_text'],
					'title_text' => $data['title_text'],
					'image_title' => $data['image_title'],
					'description' => $data['description'],
					'weight' => $data['weight'],
				);
			}
			$add_query1 = 'INSERT INTO {slideshow_framework_images} (nid, fid, alt_text, title_text, image_title, description, weight, hash) VALUES ';
			$add_query2 = '';
			$arguments = array();
			$del_fids = array();
			$status_0_fids = array();
			
			foreach(element_children($node) as $index)
			{
				if(preg_match('/^slideshow_framework_image_([0-9]+)$/', $index, $matches))
				{
					$image_fid = 'image_id_' . $matches[1];
					$image_index = $matches[0];
					$alt_index = 'slideshow_framework_alt_text_' . $matches[1];
					$title_index = 'slideshow_framework_title_text_' . $matches[1];
					$image_title_index = 'slideshow_framework_image_title_' . $matches[1];
					$description_index = 'slideshow_framework_description_' . $matches[1];
					$weight_index = 'weight_' . $matches[1];
					$submit_action = $node->$image_index->submit_action;
					$submit_actions = array('' => 'check_current_data', 1 => 'insert', 2 => 'delete', 3 => 'change');
					switch($submit_actions[$submit_action])
					{
						case 'check_current_data':
							if
							(
								$node->$alt_index != $current_image_data[$node->$image_fid]['alt_text'] ||
								$node->$title_index != $current_image_data[$node->$image_fid]['title_text'] ||
								$node->$image_title_index != $current_image_data[$node->$image_fid]['image_title'] ||
								$node->$description_index != $current_image_data[$node->$image_fid]['description'] ||
								$node->$weight_index != $current_image_data[$node->$image_fid]['weight']
							)
							{
								db_query('UPDATE {slideshow_framework_images} SET alt_text = "%s", title_text = "%s", image_title = "%s", description = "%s", weight = %d WHERE nid = %d AND fid = %d', $node->$alt_index, $node->$title_index, $node->$image_title_index, $node->$description_index, $node->$weight_index, $node->nid, $node->$image_index->fid);
							}
							break;
						case 'insert':
							$image_id = 0;
							$image_id = upload_element_save($node->$image_index, 'slideshow_framework_images', FILE_EXISTS_RENAME);
							if($image_id)
							{
								if(strlen($add_query2))
								{
									$add_query2 .= ', ';
								}
								$add_query2 .= '(%d, %d, "%s", "%s", "%s", "%s", %d, "%s")';
								$arguments[] = $node->nid;
								$arguments[] = $image_id;
								$arguments[] = $node->$alt_index;
								$arguments[] = $node->$title_index;
								$arguments[] = $node->$image_title_index;
								$arguments[] = $node->$description_index;
								$arguments[] = $node->$weight_index;
								$hash = sha1(_slideshow_framework_str_rand());
								$hash_exists = db_result(db_query('SELECT 1 FROM {slideshow_framework_images} WHERE hash = "%s" LIMIT 1', $hash));
								while($hash_exists)
								{
									$hash = sha1(_slideshow_framework_str_rand());
									$hash_exists = db_result(db_query('SELECT 1 FROM {slideshow_framework_images} WHERE hash = "%s" LIMIT 1', $hash));
								}
								$arguments[] = $hash;
							}
							break;
						case 'delete':
							$status_0_fids[] = $node->$image_index->fid;
							$del_fids[] = $node->$image_index->fid;
							break;
						case 'change':
							$status_0_fids[] = $node->$image_index->original_fid;
							$image_id = 0;
							$image_id = upload_element_save($node->$image_index, 'slideshow_framework_images', FILE_EXISTS_RENAME);
							if($image_id)
							{
								db_query('UPDATE {slideshow_framework_images} SET fid = %d, alt_text = "%s", title_text = "%s", image_title = "%s", description = "%s", weight = %d WHERE nid = %d AND fid = %d', $image_id, $node->$alt_index, $node->$title_index, $node->$image_title_index, $node->$description_index, $node->$weight_index, $node->nid, $node->$image_index->original_fid);
							}
							break;
					}
				}
			}
			if(strlen($add_query2))
			{
				db_query($add_query1 . $add_query2, $arguments);
			}
			if(count($status_0_fids))
			{
				$fids = implode(',', $status_0_fids);
				$db_files = db_query('SELECT * FROM {files} WHERE fid IN (%s)', $fids);
				while($file = db_fetch_object($db_files))
				{
					file_set_status($file, 0);
				}
			}
			if(count($del_fids))
			{
				db_query('DELETE FROM {slideshow_framework_images} WHERE nid = %d AND fid IN (%s)', $node->nid, implode(',', $del_fids));
			}
			db_query('UPDATE {slideshow_framework_slideshows} SET allow_downloads = %d, private_downloads = %d WHERE nid = %d', $node->slideshow_framework_download_link, $node->slideshow_framework_secure_downloads, $node->nid);
		}
		elseif($op == 'delete')
		{
			$db_files = db_query('SELECT f.* FROM {files} AS f JOIN {slideshow_framework_images} AS nsi ON nsi.fid = f.fid WHERE nsi.nid = %d', $node->nid);
			while($file = db_fetch_object($db_files))
			{
				file_set_status($file, 0);
			}
			db_query('DELETE FROM {slideshow_framework_images} WHERE nid = %d', $node->nid);
		}
	}
}


/**
 * Implementation of hook_form_alter()
 */
function slideshow_framework_form_alter(&$form, &$form_state, $form_id)
{
	$node_types = variable_get('slideshow_framework_content_types', array());
	preg_match('/^([_0-9a-zA-Z]*)_node_form$/', $form_id, $matches);
	if(isset($matches[1]) && in_array($matches[1], $node_types, TRUE))
	{
		$allow_alt_text = variable_get('slideshow_framework_allow_alt_text', 0);
		$allow_title_text = variable_get('slideshow_framework_allow_title_text', 0);
		$allow_image_title = variable_get('slideshow_framework_allow_image_title', 1);
		$allow_description_text = variable_get('slideshow_framework_allow_description', 1);
		$form['slideshow_fieldset'] = array
		(
			'#type' => 'fieldset', 
			'#title' => t('Slideshow Images'),
			'#collapsible' => TRUE,
			'#attributes' => array('id' => 'slideshow_images_fieldset'),
		);
		$form['slideshow_fieldset']['images_wrapper'] = array
		(
			'#prefix' => '<div id="slideshow_images_wrapper">',
			'#suffix' => '</div>',
			'#theme' => 'slideshow_framework_tabledrag',
		);
		if($form_state['add_element'])
		{
			foreach(element_children($form_state['values']) as $element_name)
			{
				if(preg_match('/^weight_([0-9]+)$/', $element_name, $weight_matches))
				{
					$values[$weight_matches[1]] = $form_state['values'][$weight_matches[0]];
				}
			}
			asort($values, SORT_NUMERIC);
			$delta = count($values) + 1;
			$i = 0;
			$weight = 0;
			$index = 0;
			foreach(array_keys($values) as $id)
			{
				$index = ($id > $index) ? $id : $index;
				$weight = ($form_state['values']['weight_' . $id] > $weight) ? $form_state['values']['weight_' . $id] : $weight;
				$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_' . $id] = array
				(
					'#type' => 'fieldset',
					'#title' => t('Image !i', array('!i' => $i + 1)),
					'#collapsible' => TRUE,
				);
				$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_' . $id]['slideshow_framework_image_' . $id] = array
				(
					'#prefix' => '<div class="slideshow_framework_upload_element">',
					'#suffix' => '</div>',
					'#type' => 'image_upload_element',
					'#title' => t('Slideshow Image !i', array('!i' => $i + 1)),
					'#default_value' => $form_state['values']['slideshow_framework_image_' . $id],
				);
				if($allow_alt_text)
				{
					$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_' . $id]['slideshow_framework_alt_text_' . $id] = array
					(
						'#prefix' => '<div class="slideshow_framework_alt_text">',
						'#suffix' => '</div>',
						'#type' => 'textfield',
						'#title' => t('Alt text for image !i', array('!i' => $i + 1)),
						'#default_value' => $form_state['values']['slideshow_framework_alt_text_' . $id],
						'#maxlength' => 128,
						'#description' => t('Enter the alt text for the image. This text will be placed inside the "alt" attribute of the images HTML tag.'),
					);
				}
				if($allow_title_text)
				{
					$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_' . $id]['slideshow_framework_title_text_' . $id] = array
					(
						'#prefix' => '<div class="slideshow_framework_title_text">',
						'#suffix' => '</div>',
						'#type' => 'textfield',
						'#title' => t('Title text for image !i', array('!i' => $i + 1)),
						'#default_value' => $form_state['values']['slideshow_framework_title_text_' . $id],
						'#maxlength' => 128,
						'#description' => t('Enter the title text for the image. This text will be placed inside the "title" attribute of the images HTML tag, and will pop up when the user hovers the mouse over the image.'),
					);
				}
				if($allow_image_title)
				{
					$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_' . $id]['slideshow_framework_image_title_' . $id] = array
					(
						'#prefix' => '<div class="slideshow_framework_image_title">',
						'#suffix' => '</div>',
						'#type' => 'textfield',
						'#title' => t('Image title'),
						'#default_value' => $form_state['values']['slideshow_framework_image_title_' . $id],
						'#maxlength' => 128,
						'#description' => t('Enter the title of the image'),
					);
				}	
				if($allow_description_text)
				{
					$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_' . $id]['slideshow_framework_description_' . $id] = array
					(
						'#prefix' => '<div class="slideshow_framework_description">',
						'#suffix' => '</div>',
						'#type' => 'textarea',
						'#title' => t('Description for image !i', array('!i' => $i + 1)),
						'#rows' => 2,
						'#default_value' => $form_state['values']['slideshow_framework_description_' . $id],
						'#description' => t('Enter the description for the image.'),
					);
				}
				$form['slideshow_fieldset']['images_wrapper']['weight_' . $id] = array
				(
					'#type' => 'weight',
					'#delta' => $delta,
					'#default_value' => $form_state['values']['weight_' . $id],
				);
				if(isset($form_state['values']['image_id_' . $id]))
				{
					$form['image_id_' . $id] = array
					(
						'#type' => 'value',
						'#value' => $form_state['values']['image_id_' . $id]
					);
				}
				$i++;
			}
			$index++;
			$weight++;
			$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_' . $index] = array
			(
				'#type' => 'fieldset',
				'#title' => t('Image !i', array('!i' => $i + 1)),
				'#collapsible' => TRUE,
			);
			$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_' . $index]['slideshow_framework_image_' . $index] = array
			(
				'#prefix' => '<div class="slideshow_framework_upload_element">',
				'#suffix' => '</div>',
				'#type' => 'image_upload_element',
				'#title' => t('Slideshow Image !i', array('!i' => $i + 1)),
			);
			if($allow_alt_text)
			{
				$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_' . $index]['slideshow_framework_alt_text_' . $index] = array
				(
					'#prefix' => '<div class="slideshow_framework_alt_text">',
					'#suffix' => '</div>',
					'#type' => 'textfield',
					'#title' => t('Alt text for image !i', array('!i' => $i + 1)),
					'#maxlength' => 128,
					'#description' => t('Enter the alt text for the image. This text will be placed inside the "alt" attribute of the images HTML tag.'),
				);
			}
			if($allow_title_text)
			{
				$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_' . $index]['slideshow_framework_title_text_' . $index] = array
				(
					'#prefix' => '<div class="slideshow_framework_title_text">',
					'#suffix' => '</div>',
					'#type' => 'textfield',
					'#title' => t('Title text for image !i', array('!i' => $i + 1)),
					'#maxlength' => 128,
					'#description' => t('Enter the title text for the image. This text will be placed inside the "title" attribute of the images HTML tag, and will pop up when the user hovers the mouse over the image.'),
				);
			}
			if($allow_image_title)
			{
				$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_' . $index]['slideshow_framework_image_title_' . $index] = array
				(
					'#prefix' => '<div class="slideshow_framework_image_title">',
					'#suffix' => '</div>',
					'#type' => 'textfield',
					'#title' => t('Image title'),
					'#maxlength' => 128,
					'#description' => t('Enter the title of the image'),
				);
			}
			if($allow_description_text)
			{
				$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_' . $index]['slideshow_framework_description_' . $index] = array
				(
					'#prefix' => '<div class="slideshow_framework_description">',
					'#suffix' => '</div>',
					'#type' => 'textarea',
					'#title' => t('Description for image !i', array('!i' => $i + 1)),
					'#rows' => 2,
					'#description' => t('Enter the description for the image.'),
				);
			}
			$form['slideshow_fieldset']['images_wrapper']['weight_' . $index] = array
			(
				'#type' => 'weight',
				'#delta' => $delta,
				'#default_value' => $weight,
			);
		}
		elseif($form['#node']->slideshow_framework_fids && count($form['#node']->slideshow_framework_fids))
		{
			$i = 0;
			$delta = count($form['#node']->slideshow_framework_fids) + 1;
			$db_image_data = db_query
			(
				'SELECT fid, alt_text, title_text, image_title, description ' .
				'FROM {slideshow_framework_images} ' .
				'WHERE fid IN (' . implode(',', $form['#node']->slideshow_framework_fids) . ')'
			);
			while($data = db_fetch_array($db_image_data))
			{
				$image_data[$data['fid']] = array
				(
					'alt_text' => $data['alt_text'],
					'title_text' => $data['title_text'],
					'image_title' => $data['image_title'],
					'description' => $data['description'],
				);
			}
			foreach($form['#node']->slideshow_framework_fids as $fid)
			{
				$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_' . $i] = array
				(
					'#type' => 'fieldset',
					'#title' => t('Image !i', array('!i' => $i + 1)),
					'#collapsible' => TRUE,
				);
				$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_' . $i]['slideshow_framework_image_' . $i] = array
				(
					'#prefix' => '<div class="slideshow_framework_upload_element">',
					'#suffix' => '</div>',
					'#type' => 'image_upload_element',
					'#title' => t('Slideshow image !i', array('!i' => $i + 1)),
					'#default_value' => db_fetch_object(db_query('SELECT * FROM files WHERE fid = %d', $fid)),
				);
				if($allow_alt_text)
				{
					$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_' . $i]['slideshow_framework_alt_text_' . $i] = array
					(
						'#prefix' => '<div class="slideshow_framework_alt_text">',
						'#suffix' => '</div>',
						'#type' => 'textfield',
						'#title' => t('Alt text for image !i', array('!i' => $i + 1)),
						'#default_value' => $image_data[$fid]['alt_text'],
						'#maxlength' => 128,
						'#description' => t('Enter the alt text for the image. This text will be placed inside the "alt" attribute of the images HTML tag.'),
					);
				}
				if($allow_title_text)
				{
					$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_' . $i]['slideshow_framework_title_text_' . $i] = array
					(
						'#prefix' => '<div class="slideshow_framework_title_text">',
						'#suffix' => '</div>',
						'#type' => 'textfield',
						'#title' => t('Title text for image !i', array('!i' => $i + 1)),
						'#default_value' => $image_data[$fid]['title_text'],
						'#maxlength' => 128,
						'#description' => t('Enter the title text for the image. This text will be placed inside the "title" attribute of the images HTML tag, and will pop up when the user hovers the mouse over the image.'),
					);
				}
				if($allow_image_title)
				{
					$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_' . $i]['slideshow_framework_image_title_' . $i] = array
					(
						'#prefix' => '<div class="slideshow_framework_image_title">',
						'#suffix' => '</div>',
						'#type' => 'textfield',
						'#title' => t('Image title'),
						'#maxlength' => 128,
						'#default_value' => $image_data[$fid]['image_title'],
						'#description' => t('Enter the title of the image'),
					);
				}
				if($allow_description_text)
				{
					$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_' . $i]['slideshow_framework_description_' . $i] = array
					(
						'#prefix' => '<div class="slideshow_framework_description">',
						'#suffix' => '</div>',
						'#type' => 'textarea',
						'#title' => t('Description for image !i', array('!i' => $i + 1)),
						'#rows' => 2,
						'#default_value' => $image_data[$fid]['description'],
						'#description' => t('Enter the description for the image.'),
					);
				}
				$form['slideshow_fieldset']['images_wrapper']['weight_' . $i] = array
				(
					'#type' => 'weight',
					'#delta' => $delta,
					'#default_value' => db_result(db_query('SELECT weight FROM {slideshow_framework_images} WHERE fid = %d', $fid)),
				);
				$form['image_id_' . $i] = array
				(
					'#type' => 'value',
					'#value' => $fid,
				);
				$i++;
			}
		}
		else
		{
			$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_0'] = array
			(
				'#type' => 'fieldset',
				'#title' => t('Image !i', array('!i' => 1)),
				'#collapsible' => TRUE,
			);
			$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_0']['slideshow_framework_image_0'] = array
			(
				'#prefix' => '<div class="slideshow_framework_upload_element">',
				'#suffix' => '</div>',
				'#type' => 'image_upload_element',
				'#title' => t('Slideshow Image !i', array('!i' => 1)),
				'#id' => 'slideshow_framework_image_0',
			);
			if($allow_alt_text)
			{
				$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_0']['slideshow_framework_alt_text_0'] = array
				(
					'#prefix' => '<div class="slideshow_framework_alt_text">',
					'#suffix' => '</div>',
					'#type' => 'textfield',
					'#title' => t('Alt text for image !i', array('!i' => $i + 1)),
					'#maxlength' => 128,
					'#description' => t('Enter the alt text for the image. This text will be placed inside the "alt" attribute of the images HTML tag.'),
				);
			}
			if($allow_title_text)
			{
				$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_0']['slideshow_framework_title_text_0'] = array
				(
					'#prefix' => '<div class="slideshow_framework_title_text">',
					'#suffix' => '</div>',
					'#type' => 'textfield',
					'#title' => t('Title text for image !i', array('!i' => $i + 1)),
					'#maxlength' => 128,
					'#description' => t('Enter the title text for the image. This text will be placed inside the "title" attribute of the images HTML tag, and will pop up when the user hovers the mouse over the image.'),
				);
			}
			if($allow_image_title)
			{
				$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_0']['slideshow_framework_image_title_0'] = array
				(
					'#prefix' => '<div class="slideshow_framework_image_title">',
					'#suffix' => '</div>',
					'#type' => 'textfield',
					'#title' => t('Image title'),
					'#maxlength' => 128,
					'#description' => t('Enter the title of the image'),
				);
			}
			if($allow_description_text)
			{
				$form['slideshow_fieldset']['images_wrapper']['slideshow_framework_image_fieldset_0']['slideshow_framework_description_0'] = array
				(
					'#prefix' => '<div class="slideshow_framework_description">',
					'#suffix' => '</div>',
					'#type' => 'textarea',
					'#title' => t('Description for image !i', array('!i' => $i + 1)),
					'#rows' => 2,
					'#description' => t('Enter the description for the image.'),
				);
			}
			$form['slideshow_fieldset']['images_wrapper']['weight_0'] = array
			(
				'#type' => 'weight',
				'#delta' => 0,
				'#default_value' => 0,
			);
		}
		$form['slideshow_fieldset']['add_slideshow_image'] = array
		(
			'#type' => 'submit',
			'#value' => t('Add another image'),
			'#skip_validation' => TRUE,
			'#submit' => array(),
			'#ahah' => array
			(
				'path' => 'slideshow_framework/ahah_callback',
				'wrapper' => 'slideshow_images_wrapper',
			),
		);
		$form['downloads'] = array
		(
			'#type' => 'fieldset',
			'#title' => t('Image Downloads'),
			'#collapsible' => TRUE,
		);
		$form['downloads']['slideshow_framework_download_link'] = array
		(
			'#title' => t('Add download link to images'),
			'#type' => 'checkbox',
			'#default_value' => $form['#node']->slideshow_allow_downloads,
			'#description' => t('If this box is checked, a download link will be provided for each image.'),
		);
		$form['downloads']['slideshow_framework_secure_downloads'] = array
		(
			'#title' => t('Make downloads secure'),
			'#type' => 'checkbox',
			'#default_value' => $form['#node']->slideshow_private_downloads,
			'#description' => t('If download links are turned on, and this box is checked, only users with permission to view the slideshow node will be able to download the images in this slideshow, and only when they are signed in.'),
		);
		$form['#after_build'][] = 'slideshow_framework_after_build';
	}
}

function slideshow_framework_after_build($form, &$form_state)
{
	$form['buttons']['submit']['#submit'] = array('node_form_submit');
	return $form;
}

/**
 * Theme function for the node creation page. This theme function
 * adds tabledrag functionality to the image upload elements on the
 * node creation page.
 */
function theme_slideshow_framework_tabledrag($form)
{
	$path = drupal_get_path('module', 'slideshow_framework');
	drupal_add_js($path . '/module_scripts/form.js');
	drupal_add_css($path . '/module_css/form.css');
	drupal_add_tabledrag('slideshow_framework_draggable_images_table', 'order', 'sibling', 'weight-group');
	$header = array(t('Image'), t('Weight'));
	$rows = array();
	foreach(element_children($form) as $key)
	{
		if(preg_match('/^slideshow_framework_image_fieldset_([0-9]+)$/', $key, $image_key))
		{
			$row = array();	
			$row[] = drupal_render($form['slideshow_framework_image_fieldset_' . $image_key[1]]);
		}
		else
		{
			preg_match('/^weight_([0-9]+)$/', $key, $weight_key);
			$form['weight_' . $weight_key[1]]['#attributes']['class'] = 'weight-group';
			$row[] = drupal_render($form['weight_' . $weight_key[1]]);
			$rows[] = array('data' => $row, 'class' => 'draggable');
		}
	}
	$output = theme('table', $header, $rows, array('id' => 'slideshow_framework_draggable_images_table'));
	return $output . drupal_render($form);
}

/**
 * Callback function for the path 'slideshow_framework/ahah_callback'.
 * This callback tells the node creation page to add an extra image upload element
 */
function slideshow_framework_add_image_element()
{
	$form = ahah_render();
	
	$changed_elements = $form['slideshow_fieldset']['images_wrapper'];
	unset($changed_elements['#prefix'], $changed_elements['#suffix']);
	$output = drupal_render($changed_elements);
	
	$javascript = drupal_add_js(NULL, NULL);
	if(isset($javascript['setting']))
	{
		$output .= '<script type="text/javascript">jQuery.extend(Drupal.settings, '. drupal_to_js(call_user_func_array('array_merge_recursive', $javascript['setting'])) .');</script>';
	}
	print drupal_to_js(array('data' => $output, 'status' => true));
	exit();
}

/**
 * Rendering function for the AHAH callback function. This function is called in 
 * slideshow_framework_add_image_element().
 */
function ahah_render()
{
	include_once 'modules/node/node.pages.inc';
	$form_state = array('storage' => NULL, 'submitted' => FALSE);
	$form_build_id = $_POST['form_build_id'];
	
	
	// Get the form from the cache.
	$form = form_get_cache($form_build_id, $form_state);
	$args = $form['#parameters'];
	$form_id = array_shift($args);
	
	
	// We need to process the form, prepare for that by setting a few internals.
	$form_state['post'] = $form['#post'] = $_POST;
	$form['#programmed'] = $form['#redirect'] = FALSE;
	
	
	$form_state['add_element'] = TRUE;
	
	
	// Build, validate and if possible, submit the form.
	drupal_process_form($form_id, $form, $form_state);
	
	
	// If validation fails, force form submission - this is my own "hack" for overcoming
	// issues where all required fields need to be filled out before the 'add more' button
	// can be clicked.  A better solution is being worked on in Drupal's issue queue.
	if (form_get_errors())
	{
		form_execute_handlers('submit', $form, $form_state);
	}
	
	
	// This call recreates the form relying solely on the form_state that the
	// drupal_process_form set up.
	$form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
	
	return $form;
}


/**
 * Implementation of hook_theme()
 */
function slideshow_framework_theme()
{
	$path = drupal_get_path('module', 'slideshow_framework') . '/templates';
	return array
	(
		'slideshow_framework_thumb' => array
		(
			'arguments' => array
			(
				'image' => NULL,
				'link' => NULL,
				'selected' => NULL,
				'download_link' => NULL,
			),
			'path' => $path,
			'template' => 'slideshow-framework-thumb',
		),
		'slideshow_framework_thumbnails' => array
		(
			'arguments' => array
			(
				'thumbnails' => NULL,
				'teaser' => NULL,
			),
			'path' => $path,
			'template' => 'slideshow-framework-thumbnails',
		),
		'slideshow_framework_image_titles' => array
		(
			'arguments' => array
			(
				'image_titles' => NULL,
			),
			'path' => $path,
			'template' => 'slideshow-framework-image-titles',
		),
		'slideshow_framework_descriptions' => array
		(
			'arguments' => array
			(
				'descriptions' => NULL,
			),
			'path' => $path,
			'template' => 'slideshow-framework-image-descriptions',
		),
		'slideshow_framework' => array
		(
			'arguments' => array
			(
				'image_data' => NULL,
				'allow_downloads' => NULL,
				'selected_image_id' => NULL,
				'nid' => NULL,
				'teaser' => NULL,
			),
			'path' => $path,
			'template' => 'slideshow-framework',
		),
		'slideshow_framework_gallery_page' => array
		(
			'arguments' => array
			(
				'slideshows' => NULL,
				'pager' => NULL,
			),
			'path' => $path,
			'template' => 'slideshow-framework-gallery',
		),
		'slideshow_framework_tabledrag' => array
		(
			'arguments' => array
			(
				'form' => NULL,
			),
		),
	);
}

/**
 * Preproccess function for the slideshow.
 * This function takes all the images, resizes them into images and thumbnails according to their imagecache presets,
 * adds the relevant javascript and CSS,
 * adds javascript settings,
 * builds the HTML for the preloaded images,
 * and themes the thumbnails.
 */
function template_preprocess_slideshow_framework(&$vars)
{
	global $base_url;
	$vars['display_slideshow'] = TRUE;
	$path = drupal_get_path('module', 'slideshow_framework');
	$settings = array('transitionImages' => variable_get('slideshow_framework_transition_images', 1), 'fadeSpeed' => variable_get('slideshow_framework_fade_speed', 500), 'transitionSpeed' => variable_get('slideshow_framework_transition_speed', 5000));
	drupal_add_js(array('slideshowFramework' => $settings), 'setting');
	drupal_add_js(variable_get('slideshow_framework_js_file', 'sites/all/modules/slideshow_framework/slideshow_css/slideshow_framework.js'));
	drupal_add_css(variable_get('slideshow_framework_css_file', 'sites/all/modules/slideshow_framework/slideshow_css/slideshow_framework.css'));
	$vars['titles_exist'] = FALSE;
	$vars['descriptions_exist'] = FALSE;
	if(isset($vars['image_data']) && is_array($vars['image_data']))
	{
		$image_preset = variable_get('slideshow_framework_image_preset', 'slideshow_framework_image');
		$thumb_preset = variable_get('slideshow_framework_thumb_preset', 'slideshow_framework_thumb');
		foreach($vars['image_data'] AS $id => $image)
		{
			if(strlen($image['image_title']))
			{
				$vars['titles_exist'] = TRUE;
			}
			if(strlen($image['description']))
			{
				$vars['descriptions_exist'] = TRUE;
			}
			if($vars['selected_image_id'] == $id || (!$vars['selected_image_id'] && !isset($vars['selected_image'])))
			{
				$image_titles[] = '<div class="slideshow_framework_image_title selected">' . $image['image_title'] . '</div>';
				$descriptions[] = '<div class="slideshow_framework_image_description selected"><p>' . str_replace('<br />', '</p><p>', nl2br($image['description'])) . '</p></div>';
				if($image_preset !== -1)
				{
					$vars['selected_image']= theme('imagecache', $image_preset, $image['filepath'], $image['alt_text'], $image['title_text']);
				}
				else
				{
					$vars['selected_image'] = '<img src="' . $base_url . '/' . $image['filepath'] . '" alt="' . $image['alt_text'] . '" title="' . $image['title_text'] . '" />';
				}
				$selected = ' selected';
				$preload_images[] = '<div id="placeholder"></div>';
			}
			else
			{
				$image_titles[] = '<div class="slideshow_framework_image_title">' . $image['image_title'] . '</div>';
				$descriptions[] = '<div class="slideshow_framework_image_description"><p>' . str_replace('<br />', '</p><p>', nl2br($image['description'])) . '</p></div>';
				if($image_preset != -1)
				{
					$preload_images[] = theme('imagecache', $image_preset, $image['filepath'], $image['alt_text'], $image['title_text']);
				}
				else
				{
					$preload_images[] = '<img src="' . $base_url . '/' . $image['filepath'] . '" alt="' . $image['alt_text'] . '" title="' . $image['title_text'] . '" />';
				}
				$selected = '';
			}
			if($thumb_size != -1)
			{
				$thumb = theme('imagecache', $thumb_preset, $image['filepath']);
			}
			else
			{
				$thumb = '<img src="' . $base_url . '/' . $image['filepath'] . '" alt="" />';
			}
			$link = url($_GET['q'], array('absolute' => TRUE, 'query' => 'image=' . $id));
			$download_url = ($vars['allow_downloads']) ? url('slideshow_framework/download/' . $vars['nid'] . '/' . $image['hash'], array('absolute' => TRUE)) :  FALSE;
			if($selected)
			{
				$thumbs['selected'] = theme('slideshow_framework_thumb', $thumb, $link, $selected, $download_url);
			}
			else
			{
				$thumbs[] = theme('slideshow_framework_thumb', $thumb, $link, $selected, $download_url);
			}
		}
		$i = 0;
		if(count($thumbs))
		{
			foreach($thumbs as $key => $thumb)
			{
				if($key === 'selected')
				{
					break;
				}
				$i++;
			}
			for($j = 0; $j < $i; $j++)
			{
				$temp = array_shift($thumbs);
				$thumbs[] = $temp;
				$temp = array_shift($preload_images);
				$preload_images[] = $temp;
				$temp = array_shift($image_titles);
				$image_titles[] = $temp;
				$temp = array_shift($descriptions);
				$descriptions[] = $temp;
			}
			$vars['thumbs'] = theme('slideshow_framework_thumbnails', $thumbs, $vars['teaser']);
			$vars['preload_images'] = implode('', $preload_images);
			$vars['image_titles'] = theme('slideshow_framework_image_titles', $image_titles);
			$vars['descriptions'] = theme('slideshow_framework_descriptions', $descriptions);
		}
		else
		{
			$vars['display_slideshow'] = FALSE;
		}
	}
	else
	{
		$vars['display_slideshow'] = FALSE;
	}
}

/**
 * Creates a random hash of the length specified
 */
function _slideshow_framework_str_rand($length = 16)
{
	$seeds = 'abcdefghijklmnopqrstuvwqyz0123456789';
	
	// Seed generator
	list($usec, $sec) = explode(' ', microtime());
	$seed = (float) $sec + ((float) $usec * 100000);
	mt_srand($seed);
	
	// Generate
	$str = '';
	$seeds_count = strlen($seeds);
	
	for ($i = 0; $length > $i; $i++)
	{
		$str .= $seeds{mt_rand(0, $seeds_count - 1)};
	}
	
	return $str;
}

