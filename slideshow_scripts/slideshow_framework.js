
var slideshowFramework = {};
slideshowFramework.current = 0;
slideshowFramework.total= 0;
slideshowFramework.status = 1;
slideshowFramework.timeout = false;
slideshowFramework.titlesExist = false;
slideshowFramework.descriptionsExist = false;

slideshowFramework.buttons = function()
{
	slideshowFramework.setButtonFaders();
	slideshowFramework.setButtonListeners();
};

slideshowFramework.setButtonFaders = function()
{
	$("#selected_image").hover
	(
		function()
		{
			$("#slideshow_left_button, #slideshow_right_button").fadeIn()
		},
		function()
		{
			$("#slideshow_left_button, #slideshow_right_button").fadeOut()
		}
	);
};

slideshowFramework.setButtonListeners = function()
{
	$("#slideshow_left_button a").click(function()
	{
		if(slideshowFramework.status)
		{
			slideshowFramework.current = ((slideshowFramework.current - 1) >= 0) ? slideshowFramework.current - 1 : (slideshowFramework.total - 1);
			slideshowFramework.exchangeImage();
		}
		return false;
	});
	$("#slideshow_right_button a").click(function()
	{
		if(slideshowFramework.status)
		{
			slideshowFramework.current = ((slideshowFramework.current + 1) < slideshowFramework.total) ? slideshowFramework.current + 1 : 0;
			slideshowFramework.exchangeImage();
		}
		return false;
	});
};

slideshowFramework.exchangeImage = function()
{
	if(slideshowFramework.status)
	{
		slideshowFramework.status = 0;
		clearTimeout(slideshowFramework.timeout);
		if(slideshowFramework.titlesExist)
		{
			$("#slideshow_framework_image_titles .slideshow_framework_image_title").slideUp(Drupal.settings.slideshowFramework.fadeSpeed);
			$(".slideshow_framework_image_title:eq(" + slideshowFramework.current + ")").slideDown(Drupal.settings.slideshowFramework.fadeSpeed);
		}
		if(slideshowFramework.descriptionsExist)
		{
			$("#slideshow_framework_image_descriptions .slideshow_framework_image_description").slideUp(Drupal.settings.slideshowFramework.fadeSpeed);
			$(".slideshow_framework_image_description:eq(" + slideshowFramework.current + ")").slideDown(Drupal.settings.slideshowFramework.fadeSpeed);
		}
		$("#selected_image img").fadeOut(Number(Drupal.settings.slideshowFramework.fadeSpeed), function()
		{
			$("#placeholder").replaceWith($("#selected_image img")).appendTo("#selected_image");
			$("#preloaded_images img:eq(" + slideshowFramework.current + ")").css("display", "none").replaceWith($("#placeholder")).appendTo("#selected_image").fadeIn(Number(Drupal.settings.slideshowFramework.fadeSpeed), function()
			{
				slideshowFramework.status = 1;
			})
			if(Number(Drupal.settings.slideshowFramework.transitionImages))
			{
				slideshowFramework.timeout = setTimeout("slideshowFramework.advance()", Number(Drupal.settings.slideshowFramework.transitionSpeed));
			}
		});
		slideshowFramework.moveIndicator();
	}
};

slideshowFramework.moveIndicator = function()
{
	if((slideshowFramework.total - 7 ) > 0)
	{
		var factor = (523 / (slideshowFramework.total - 1));
	}
	else
	{
		var factor = 87;
	}
	var indicatorPos = slideshowFramework.current * factor;
	var tDuration = (Number(Drupal.settings.slideshowFramework.fadeSpeed) * 2);
	$("#thumb_indicator").animate
	(	
	 	{
	 		top : indicatorPos
		},
		tDuration,
		"swing"
	);
	if((slideshowFramework.total - 7) > 0)
	{
		var factor = ((slideshowFramework.total - 7) / (slideshowFramework.total - 1)) * 87 * -1;
	}
	else
	{
		var factor = 0;
	}
	var sliderPos = slideshowFramework.current * factor;
	$("#slider").animate
	(	
	 	{
			top : sliderPos,
		},
		tDuration,
		"swing"
	);
};

slideshowFramework.thumbs = function()
{
	$("#thumbs .thumb img").each(function() 
	{
		$(this).click(function()
		{
			if(slideshowFramework.status)
			{
				slideshowFramework.current = $("#thumbs .thumb img").index(this);
				slideshowFramework.exchangeImage();
			}
			return false;
		});
	});
	$("#thumbs a").each(function()
	{
		// remove dotted outline in FF and other browsers
		$(this).css("outline", "none");
		// remove dotted outline in IE
		$(this).focus(function()
		{
			$(this).blur();
		});
	})
};

slideshowFramework.advance = function()
{
	if(slideshowFramework.status)
	{
		slideshowFramework.current = ((slideshowFramework.current + 1) < slideshowFramework.total) ? slideshowFramework.current + 1 : 0;
		slideshowFramework.exchangeImage();
	}
};

slideshowFramework.downloadLinks = function()
{
	$(".thumb").hover
	(
		function()
		{
			$(this).children(".image_download_link").fadeIn('fast');
		},
		function()
		{
			$(this).children(".image_download_link").fadeOut('fast');
		}
	);
};

Drupal.behaviors.slideshowFramework = function()
{
	// initialize the slideshow
	slideshowFramework.total = $("#preloaded_images img").length + 1;
	if(slideshowFramework.total > 1)
	{
		$("#selected_image img").before("<div id=\"slideshow_left_button\"><a href=\"#\"><</a></div><div id=\"slideshow_right_button\"><a href=\"#\">></a></div>")
		$("#thumbs").prepend("<div id=\"thumb_indicator\"></div>");
		$("#thumbs .thumb").css("outline", "none");
		$("#thumbs .selected img").css("border", "none");
		$("#thumbs .selected a img").css("height", "77px");
		slideshowFramework.titlesExist = $(".slideshow_framework_image_title").length;
		slideshowFramework.descriptionsExist = $(".slideshow_framework_image_description").length;
		slideshowFramework.buttons();
		slideshowFramework.thumbs();
		slideshowFramework.downloadLinks();
		if(Number(Drupal.settings.slideshowFramework.transitionImages))
		{
			slideshowFramework.timeout = setTimeout("slideshowFramework.advance()", Number(Drupal.settings.slideshowFramework.transitionSpeed));
		}
	}
}