<?php

/**
 * Callback function for the path 'admin/settings/slideshows'
 * This is the module administration page
 */
function slideshow_framework_settings($form_state)
{
	$content_types = node_get_types('names');
	$form['content_types'] = array
	(
		'#type' => 'fieldset',
		'#title' => t('Content Types'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	$form['content_types']['slideshow_framework_content_types'] = array
	(
		'#title' => t('Enable slideshow for the following content types'),
		'#type' => 'checkboxes',
		'#options' => $content_types,
		'#description' => t('Selected content types will be able to have a slideshow added to any node of that content type.'),
		'#default_value' => variable_get('slideshow_framework_content_types', array()),
	);
	$form['slideshow_framework_fade_speed'] = array
	(
		'#title' => t('Image fade speed'),
		'#type' => 'select',
		'#options' => array
		(
			1500 => t('Really slow'),
			1000 => t('Pretty slow'),
			800 => t('Slow'),
			500 => t('Normal'),
			400 => t('A little faster'),
			250 => t('Fast'),
			10 => t('No fade'),
		),
		'#default_value' => array(variable_get('slideshow_framework_fade_speed',500)),
		'#description' => t('This is the speed at which images will fade in and out'),
	);
	$form['slideshow_framework_transition_images'] = array
	(
		'#title' => t('Transition images automatically'),
		'#type' => 'radios',
		'#options' => array(1 => 'yes', 0 => 'no'),
		'#default_value' => variable_get('slideshow_framework_transition_images', 1),
	);
	$form['slideshow_framework_transition_speed'] = array
	(
		'#title' => t('Image transition speed'),
		'#type' => 'select',
		'#options' => array
		(
			3000 => t('!sec sec', array('!sec' => '3')),
			5000 => t('!sec sec', array('!sec' => '5')),
			7000 => t('!sec sec', array('!sec' => '7')),
			10000 => t('!sec sec', array('!sec' => '10')),
			15000 => t('!sec sec', array('!sec' => '15')),
			20000 => t('!sec sec', array('!sec' => '20')),
			25000 => t('!sec sec', array('!sec' => '25')),
			30000 => t('!sec sec', array('!sec' => '30')),
		),
		'#default_value' => variable_get('slideshow_framework_transition_speed', 5000),
		'#description' => t('This is the amount of time each image will show before transitioning to the next image'),
	);
	$form['files'] = array
	(
		'#type' => 'fieldset',
		'#title' => t('Slideshow Files'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	$path = drupal_get_path('module', 'slideshow_framework');
	$css_files = file_scan_directory($path . '/slideshow_css', '\.css');
	foreach($css_files as $file)
	{
		$css[$file->filename] = $file->basename;
	}
	$form['files']['slideshow_framework_css_file'] = array
	(
		'#type' => 'select',
		'#title' => t('CSS file'),
		'#options' => $css,
		'#default_value' => variable_get('slideshow_framework_css_file', 'sites/all/modules/slideshow_framework/slideshow_css/slideshow_framework.css'),
		'#description' => t('Select the !type file you want to use in your slideshow. To add a new !type file, add the file to the [MODULE_DIRECTORY]/slideshow_framework/!directory directory. Only files with the extension !extension will be shown.', array('!type' => 'CSS', '!extension' => '.css', '!directory' => 'slideshow_css')),
	);
	$js_files = file_scan_directory($path . '/slideshow_scripts', '\.js');
	foreach($js_files as $file)
	{
		$js[$file->filename] = $file->basename;
	}
	$form['files']['slideshow_framework_js_file'] = array
	(
		'#type' => 'select',
		'#title' => t('Javascript file'),
		'#options' => $js,
		'#default_value' => variable_get('slideshow_framework_js_file', 'sites/all/modules/slideshow_framework/slideshow_scripts/slideshow_framework.js'),
		'#description' => t('Select the !type file you want to use in your slideshow. To add a new !type file, add the file to the [MODULE_DIRECTORY]/slideshow_framework/!directory directory. Only files with the extension !extension will be shown.', array('!type' => 'javascript', '!extension' => '.js', '!directory' => 'slideshow_scripts')),
	);
	$form['images'] = array
	(
		'#type' => 'fieldset',
		'#title' => t('Image sizes'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	$imagecache_presets = imagecache_presets();
	foreach($imagecache_presets as $preset)
	{
		$imagecache[$preset['presetname']] = $preset['presetname'];
	}
	$imagecache[-1] = t('Original Size');

	$form['images']['slideshow_framework_image_preset'] = array
	(
		'#type' => 'select',
		'#title' => t('Image size'),
		'#options' => $imagecache,
		'#default_value' => variable_get('slideshow_framework_image_preset', 'slideshow_framework_image'),
		'#description' => t('Select the Imagecache preset for the !image_type size you would like to display. To create a new preset, ensure that the !imagecache_ui_module is installed and enabled, go to !imagecache_path, and follow the directions there to create a new preset.', array('!image_type' => t('image'), '!imagecache_ui_module' => '<a href="http://drupal.org/project/imagecache">' . t('Imagecache UI module') . '</a>', '!imagecache_path' => l(t('Imagecache settings'), 'admin/build/imagecache/add'))),
	);
	$form['images']['slideshow_framework_thumb_preset'] = array
	(
		'#type' => 'select',
		'#title' => t('Thumbnail size'),
		'#options' => $imagecache,
		'#default_value' => variable_get('slideshow_framework_thumb_preset', 'slideshow_framework_thumb'),
		'#description' => t('Select the Imagecache preset for the !image_type size you would like to display. To create a new preset, ensure that the !imagecache_ui_module is installed and enabled, go to !imagecache_path, and follow the directions there to create a new preset.', array('!image_type' => t('thumbnail'), '!imagecache_ui_module' => '<a href="http://drupal.org/project/imagecache">' . t('Imagecache UI module') . '</a>', '!imagecache_path' => l(t('Imagecache settings'), 'admin/build/imagecache/add'))),
	);
	$form['attributes'] = array
	(
		'#type' => 'fieldset',
		'#title' => t('Image options'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	$form['attributes']['header'] = array
	(
		'#value' => '<p>' . t('You can set various options for the images below. Options with checkmarks will be enabled, options without checkmarks will be disabled.') . '</p>',
	);
	$form['attributes']['slideshow_framework_allow_image_title'] = array
	(
		'#type' => 'checkbox',
		'#title' => t('Image title'),
		'#default_value' => variable_get('slideshow_framework_allow_image_title', 1),
		'#description' => t('If this box is checked, users will be able to add a title for the image. This title will be shown with the images in the slideshow.'),
	);
	$form['attributes']['slideshow_framework_allow_description'] = array
	(
		'#type' => 'checkbox',
		'#title' => t('Image description'),
		'#default_value' => variable_get('slideshow_framework_allow_description', 1),
		'#description' => t('If this box is checked, users will be able to add a description to uploaded images. This description will be shown with the images in the slideshow.'),
	);
	$form['attributes']['slideshow_framework_allow_alt_text'] = array
	(
		'#type' => 'checkbox',
		'#title' => t('Custom alt text'),
		'#default_value' => variable_get('slideshow_framework_allow_alt_text', 0),
		'#description' => t('If this box is checked, users will be able to add custom "!type" text for each image when creating a slideshow. This text will go in the HTML "!type" attribute for the image.', array('!type' => 'alt')),
	);
	$form['attributes']['slideshow_framework_allow_title_text'] = array
	(
		'#type' => 'checkbox',
		'#title' => t('Custom title text'),
		'#default_value' => variable_get('slideshow_framework_allow_title_text', 0),
		'#description' => t('If this box is checked, users will be able to add custom "!type" text for each image when creating a slideshow. This text will go in the HTML "!type" attribute for the image.', array('!type' => 'title')) . ' ' . t('This text will popup when the user hovers over the image with the mouse.'),
	);
	return system_settings_form($form);
}

function slideshow_framework_image_import($form_state)
{
	if(!isset($form_state['storage']))
	{
		$path = variable_get('file_directory_path', 'sites/default/files');
		$image_files = file_scan_directory($path . '/slideshow_framework_import', '[\.gif|\.jpg|\.jpeg|\.png]');
		$images = array();
		$preset = variable_get('slideshow_framework_thumb_preset', 'slideshow_framework_thumb');
		foreach($image_files as $image)
		{
			$images[$image->filename] = theme('imagecache', $preset, $image->filename) . $image->basename;
		}
		$path = drupal_get_path('module', 'slideshow_framework');
		drupal_add_css($path . '/module_css/import_page.css', 'module', 'all', FALSE);
		$form['header'] = array
		(
			'#value' => '<p>' . t('Select the images you would like to import into a slideshow') . '</p>',
		);
		if(count($images))
		{
			$content_types = variable_get('slideshow_framework_content_types', array());
			$slideshow_types = array();
			foreach($content_types as $type)
			{
				if($type !== 0)
				{
					$slideshow_types[$type] = $type;
				}
			}
			if(count($slideshow_types))
			{
				$slideshow_types = array(-1 => t('Select content type')) + $slideshow_types;
				$form['images'] = array
				(
					'#type' => 'checkboxes',
					'#title' => t('Images'),
					'#options' => $images,
				);
				$form['content_type'] = array
				(
					'#type' => 'select',
					'#title' => t('Import the images into a node of the following content type'),
					'#options' => $slideshow_types,
					'#default_value' => -1,
					'#required' => TRUE,
				);
				$form['submit'] = array
				(
					'#type' => 'submit',
					'#value' => t('Import'),
				);
			}
			else
			{
				$form['no_content_types'] = array
				(
					'#value' => '<p>' . t('You have not configured any content types to have slideshows attached to them. Please visit the !settings page and select at least one content type to have slideshows', array('!settings' => l(t('settings'), 'admin/settings/slideshow_framework/slideshow_framework'))) . '</p>',
				);
			}
		}
		else
		{
			$form['no_images'] = array
			(
				'#value' => t('There are no images in the import folder at this time.'),
			);
		}
	}
	elseif(isset($form_state['storage']['check_images']) && $form_state['storage']['check_images'])
	{
		$form['header'] = array
		(
			'#value' => '<p>' . t('Image !i of !count', array('!i' => $form_state['storage']['count'], '!count' => (count($form_state['storage']['images_to_check']) + $form_state['storage']['count'] - 1))) . '</p>',
		);
		$form['image'] = array
		(
			'#value' => theme('imagecache', $form_state['storage']['preset'], $form_state['storage']['images_to_check'][0]),
		);
		if($form_state['storage']['allow_image_title'])
		{
			$form['image_title'] = array
			(
				'#type' => 'textfield',
				'#title' => t('Image title'),
				'#maxlength' => 128,
				'#description' => t('Enter the title of the image'),
			);
		}
		if($form_state['storage']['allow_description'])
		{
			$form['description'] = array
			(
				'#type' => 'textarea',
				'#title' => t('Description for image'),
				'#rows' => 2,
				'#description' => t('Enter the description for the image.'),
			);
		}
		if($form_state['storage']['allow_alt'])
		{
			$form['alt_text'] = array
			(
				'#type' => 'textfield',
				'#title' => t('Alt text'),
				'#maxlength' => 128,
				'#description' => t('Enter the alt text for the image. This text will be placed inside the "alt" attribute of the images HTML tag.'),
			);
		}
		if($form_state['storage']['allow_title'])
		{
			$form['title_text'] = array
			(
				'#type' => 'textfield',
				'#title' => t('HTML Title text'),
				'#maxlength' => 128,
				'#description' => t('Enter the title text for the image. This text will be placed inside the "title" attribute of the images HTML tag, and will pop up when the user hovers the mouse over the image.'),
			);
		}
		$form['submit'] = array
		(
			'#type' => 'submit',
			'#value' => t('Submit'),
		);
	}
	else
	{
		$form['downloads']['download_link'] = array
		(
			'#title' => t('Add download link to images'),
			'#type' => 'checkbox',
			'#default_value' => 0,
			'#description' => t('If this box is checked, a download link will be provided for each image.'),
		);
		$form['downloads']['secure_downloads'] = array
		(
			'#title' => t('Make downloads secure'),
			'#type' => 'checkbox',
			'#default_value' => 0,
			'#description' => t('If download links are turned on, and this box is checked, only users with permission to view the slideshow node will be able to download the images in this slideshow, and only when they are signed in.'),
		);
		$form['submit'] = array
		(
			'#type' => 'submit',
			'#value' => t('Submit'),
		);
	}
	return $form;
}

function slideshow_framework_image_import_validate($form, &$form_state)
{
	if(!isset($form_state['storage']))
	{
		$images_exist = FALSE;
		foreach($form_state['values']['images'] as $image)
		{
			if($image)
			{
				$images_exist = TRUE;
				break;
			}
		}
		if(!$images_exist)
		{
			form_set_error('images', t("You didn't select any images for import"));
		}
		if($form_state['values']['content_type'] < 0)
		{
			form_set_error('content_type', t('Please select a content type for this slideshow'));
		}
	}
}

function slideshow_framework_image_import_submit($form, &$form_state)
{
	if(!isset($form_state['storage']))
	{
		$allow_alt = variable_get('slideshow_framework_allow_alt_text', 0);
		$allow_title = variable_get('slideshow_framework_allow_title_text', 0);
		$allow_image_title = variable_get('slideshow_framework_allow_image_title', 1);
		$allow_description = variable_get('slideshow_framework_allow_description', 1);
		foreach($form_state['values']['images'] as $image)
		{
			if($image != "0")
			{
				$images[] = $image;
			}
		}
		if($allow_alt || $allow_title || $allow_image_title || $allow_description)
		{
			$form_state['storage']['check_images'] = TRUE;
			$form_state['storage']['allow_alt'] = $allow_alt;
			$form_state['storage']['allow_title'] = $allow_title;
			$form_state['storage']['allow_image_title'] = $allow_image_title;
			$form_state['storage']['allow_description'] = $allow_description;
			$form_state['storage']['images_to_check'] = $images;
			$form_state['storage']['preset'] = variable_get('slideshow_framework_image_preset', 'slideshow_framework_image');
			$form_state['storage']['count'] = 1;
		}
		else
		{
			$form_state['storage']['checked_images'] = $images;
			$form_state['storage']['checked_image_titles'] = array();
			$form_state['storage']['checked_descriptions'] = array();
			$form_state['storage']['checked_alt_text'] = array();
			$form_state['storage']['checked_title_text'] = array();
		}
		$form_state['storage']['content_type'] = $form_state['values']['content_type'];
	}
	elseif(isset($form_state['storage']['check_images']) && $form_state['storage']['check_images'])
	{
		$form_state['storage']['checked_images'][] = array_shift($form_state['storage']['images_to_check']);
		$form_state['storage']['checked_image_titles'][] = $form_state['values']['image_title'];
		$form_state['storage']['checked_descriptions'][] = $form_state['values']['description'];
		$form_state['storage']['checked_alt_text'][] = $form_state['values']['alt_text'];
		$form_state['storage']['checked_title_text'][] = $form_state['values']['title_text'];
		$form_state['storage']['count']++;
		if(!count($form_state['storage']['images_to_check']))
		{
			unset($form_state['storage']['check_images']);
		}
	}
	else
	{
		global $user;
		$node = new StdClass;
		$path = variable_get('file_directory_path', 'sites/default/files');
		$image_directory = $path . '/slideshow_framework_images';
		file_check_directory($image_directory, TRUE);
		foreach($form_state['storage']['checked_images'] as $image)
		{
			$file = new StdClass;
			$file->uid = $user->uid;
			$pieces = explode('/', $image);
			$filename = $pieces[count($pieces) - 1];
			$file->filename = $filename;
			$file->filepath = $image;
			$file->filemime = file_get_mimetype($filename);
			$file->filesize = filesize($image);
			$file->status = 1;
			$file->timestamp = time();
			if(drupal_write_record('files', $file))
			{
				$node->slideshow_framework_fids[] = $file->fid;
				file_move($file, $image_directory . '/' . $file->filename);
			}
			else
			{
				drupal_set_message(t('There was an error importing @file', array('@file' => $file->filename)), 'error');
			}
		}
		$node->type = $form_state['storage']['content_type'];
		$node->title = t('Imported Slideshow');
		$node->uid = $user->uid;
		$node->status = 0;
		$node->slideshow_framework_alt_texts = $form_state['storage']['checked_alt_text'];
		$node->slideshow_framework_title_texts = $form_state['storage']['checked_title_text'];
		$node->slideshow_framework_image_titles = $form_state['storage']['checked_image_titles'];
		$node->slideshow_framework_descriptions = $form_state['storage']['checked_descriptions'];
		$node->slideshow_framework_download_link = $form_state['values']['download_link'];
		$node->slideshow_framework_secure_downloads = $form_state['values']['secure_downloads'];
		node_save($node);
		drupal_set_message(t('Your !slideshow has been saved. The slideshow is currently unpublished. To publish the slideshow, click "edit", scroll down to "publishing options", and check the "published" box. After saving the node, the slideshow will be visible to the public.', array('!slideshow' => l(t('slideshow'), 'node/' . $node->nid))));
	}
}

/**
 * Callback function for the path 'gallery'
 */
function slideshow_framework_gallery()
{
	$content_types = variable_get('slideshow_framework_content_types', array());
	$slideshow_content_types = array();
	foreach($content_types as $ct)
	{
		if($ct !== 0)
		{
			$slideshow_content_types[] = $ct;
		}
	}
	if(!count($slideshow_content_types))
	{
		return t('No content types have been configured to show slideshows');
	}
	$slideshow_content_types = '"' . implode('","', $slideshow_content_types) . '"';
	$db_slideshows = pager_query
	(
		db_rewrite_sql
		(
			'SELECT n.nid, n.title, n.created, u.name, u.uid ' .
			'FROM {node} AS n ' . 
			'JOIN {users} AS u ' .
			'ON u.uid = n.uid ' .
			'WHERE n.type IN (' . $slideshow_content_types . ') ' .
			'ORDER BY n.created DESC'
		)
	);
	$slideshows = array();
	while($slideshow = db_fetch_array($db_slideshows))
	{
		$filepath = db_result
		(
			db_query_range
			(
				'SELECT f.filepath ' .
				'FROM {files} AS f ' .
				'JOIN {slideshow_framework_images} AS nsi ' .
				'ON nsi.fid = f.fid ' .
				'WHERE nsi.nid = %d ' .
				'ORDER BY nsi.weight ASC ',
				$slideshow['nid'],
				0,
				1
			)
		);
		if($filepath != '')
		{
			$slideshows[] = array
			(
				'link' => url(drupal_get_path_alias('node/' . $slideshow['nid']), array('absolute' => TRUE)),
				'title' => $slideshow['title'],
				'creation_date' => format_date($slideshow['created'], 'small'),
				'username' => $slideshow['name'],
				'uid' => $slideshow['uid'],
				'thumb' => theme('imagecache', 'slideshow_framework_thumb', $filepath),
			);
		}
	}
	if(!count($slideshows))
	{
		$slideshows = FALSE;
	}
	return theme('slideshow_framework_gallery_page', $slideshows, theme('pager'));
}

/**
 * Forces the download of an image file, rather than opening it in the browser
 */
function slideshow_framework_download($hash)
{
	global $base_path;
	$file_data = db_fetch_array
	(
		db_query
		(
			'SELECT f.filepath, f.filename ' .
			'FROM {files} AS f ' .
			'JOIN {slideshow_framework_images} AS nsi ' .
			'ON nsi.fid = f.fid ' .
			'WHERE nsi.hash = "%s"',
			$hash
		)
	);
	if(!$file_data)
	{
		drupal_set_message(t('The specified file does not exist'), 'error');
		return '';
	}
	$file = realpath($file_data['filepath']);
	$size = filesize($file);
	$name = rawurldecode($file_data['filename']);
	
	/* Figure out the MIME type (if not specified) */
	$known_mime_types = array
	(
		"pdf" => "application/pdf",
		"txt" => "text/plain",
		"html" => "text/html",
		"htm" => "text/html",
		"exe" => "application/octet-stream",
		"zip" => "application/zip",
		"doc" => "application/msword",
		"xls" => "application/vnd.ms-excel",
		"ppt" => "application/vnd.ms-powerpoint",
		"gif" => "image/gif",
		"png" => "image/png",
		"jpeg"=> "image/jpg",
		"jpg" =>  "image/jpg",
		"php" => "text/plain"
	);
	
	if($mime_type=='')
	{
		$file_extension = strtolower(substr(strrchr($file,"."),1));
		if(array_key_exists($file_extension, $known_mime_types))
		{
			$mime_type = $known_mime_types[$file_extension];
		}
		else
		{
			$mime_type="application/force-download";
		}
	}
	
	@ob_end_clean(); //turn off output buffering to decrease cpu usage
	
	// required for IE, otherwise Content-Disposition may be ignored
	if(ini_get('zlib.output_compression'))
	ini_set('zlib.output_compression', 'Off');
	
	header('Content-Type: ' . $mime_type);
	header('Content-Disposition: attachment; filename="'. $file_data['filename'] .'"');
	header("Content-Transfer-Encoding: binary");
	header('Accept-Ranges: bytes');
	
	/* The three lines below basically make the 
	download non-cacheable */
	header("Cache-control: private");
	header('Pragma: private');
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	
	// multipart-download and download resuming support
	if(isset($_SERVER['HTTP_RANGE']))
	{
		list($a, $range) = explode("=",$_SERVER['HTTP_RANGE'],2);
		list($range) = explode(",",$range,2);
		list($range, $range_end) = explode("-", $range);
		$range=intval($range);
		if(!$range_end)
		{
			$range_end=$size-1;
		}
		else
		{
			$range_end=intval($range_end);
		}
	
		$new_length = $range_end-$range+1;
		header("HTTP/1.1 206 Partial Content");
		header("Content-Length: $new_length");
		header("Content-Range: bytes $range-$range_end/$size");
	}
	else
	{
		$new_length=$size;
		header("Content-Length: ".$size);
	}
	
	/* output the file itself */
	$chunksize = 1*(1024*1024); //you may want to change this
	$bytes_send = 0;
	if ($file = fopen($file, 'r'))
	{
		if(isset($_SERVER['HTTP_RANGE']))
		{
			fseek($file, $range);
		}
		while(!feof($file) && (!connection_aborted()) && ($bytes_send < $new_length))
		{
			$buffer = fread($file, $chunksize);
			print($buffer); //echo($buffer); // is also possible
			flush();
			$bytes_send += strlen($buffer);
		}
		fclose($file);
	}
	else
	{
		die('Error - can not open file.');
	}
	die();
}